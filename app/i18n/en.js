import AppError from '../utils/AppError'

export default {
  error: {
    interfaceError: 'Sorry, application crashed',
    [AppError.DUMMY_ERROR]: 'Error',
  },
  about: {
    title: 'About',
    text: 'About app..',
  },
  poloniex: {
    title: 'Tickers',
    last: 'last',
    highestBid: 'highestBid',
    percentChange: 'percentChange',
  },
}
