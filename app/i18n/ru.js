import AppError from '../utils/AppError'

export default {
  error: {
    interfaceError: 'Извините, приложение упало',
    [AppError.DUMMY_ERROR]: 'Ошибка',
  },
  about: {
    title: 'О приложении',
    text: 'О приложении..',
  },
  poloniex: {
    title: 'Котировки',
    last: 'last',
    highestBid: 'highestBid',
    percentChange: 'percentChange',
  },
}
