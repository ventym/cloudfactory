import { combineReducers } from 'redux'

import poloniex from './poloniex'

export default combineReducers({
  poloniex,
})
