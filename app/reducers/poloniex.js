import { handleActions } from 'redux-actions'
import actions from '../actions'

const initialState = {
  tickers: {},
  list: [],

  errorCode: '',
  isLoaded: false,
  isLoading: false,
  onScreen: false,
}

export default handleActions({
  [actions.poloniex.fetching]: (state, action) => ({ ...state, isLoading: true }),
  [actions.poloniex.successful]: (state, { payload }) => ({ ...state, errorCode: '', isLoading: false, isLoaded: true, tickers: payload.tickers, list: payload.list }),
  [actions.poloniex.rejected]: (state, action) => ({ ...state, errorCode: action.payload, isLoading: false }),
  [actions.poloniex.didFocus]: (state, action) => ({ ...state, onScreen: true }),
  [actions.poloniex.willBlur]: (state, action) => ({ ...state, onScreen: false }),
}, initialState)
