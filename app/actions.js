import { createActions } from 'redux-actions'

export default createActions({
  poloniex: {
    fetching: null,
    successful: null,
    rejected: null,
    didFocus: null,
    willBlur: null,
  },
})
