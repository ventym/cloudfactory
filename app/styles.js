import {
  StyleSheet,
  Dimensions,
  StatusBar,
  Platform,
} from 'react-native'

export const COLORS = {
  BACKGROUND: '#f5fcff',
  NAVIGATOR_BACKGROUND: '#2196f3',
  HIT: '#e99',
  TEXT: 'black',
  ERROR_BOX: '#edd',
  ERROR_TEXT: 'red',
  TABLE_BORDER: 'grey',
}

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BACKGROUND,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  h1: {
    fontSize: 18,
    color: COLORS.TEXT,
  },
  errorBox: {
    alignItems: 'center',
    padding: 10,
    backgroundColor: COLORS.ERROR_BOX,
  },
  errorText: {
    fontSize: 14,
    color: COLORS.ERROR_TEXT,
  },
  horizontalLine: {
    backgroundColor: COLORS.TABLE_BORDER,
    height: StyleSheet.hairlineWidth,
  },
  tickerItemContainer: {
    flexDirection: 'row',
    paddingHorizontal: 5,
    backgroundColor: COLORS.BACKGROUND,
  },
  tickerItemLeftContainer: {
    flex: 1,
    padding: 5,
  },
  tickerItemRightContainer: {
    flex: 2,
    padding: 5,
    borderLeftWidth: StyleSheet.hairlineWidth,
    borderColor: COLORS.TABLE_BORDER,
  },
  tickerName: {
    fontSize: 16,
    color: COLORS.TEXT,
  },
  tickerText: {
    fontSize: 14,
    color: COLORS.TEXT,
  },
})
