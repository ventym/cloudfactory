import React from 'react'
import {
  View,
  StatusBar,
  Platform,
} from 'react-native'

export default ({ backgroundColor, ...props }) => {
  const height = 20 // TODO: iOS X
  return Platform.OS === 'ios' ? (
    <View style={{ backgroundColor, height }}>
      <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
  ) : (
    <StatusBar backgroundColor={backgroundColor} {...props} />
  )
}
