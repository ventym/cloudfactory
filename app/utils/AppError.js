function AppError(errorCode){
  this.name = 'AppError'
  this.message = 'AppError ' + errorCode
  this.errorCode = errorCode
  this.stack = new Error().stack
}
AppError.prototype = Object.create(Error.prototype)

AppError.DUMMY_ERROR = 'DUMMY_ERROR'
AppError.API_ERROR = 'API_ERROR'
AppError.ACCESS_DENIED = 'ACCESS_DENIED'

export default AppError

export function prepareError(error) {
  if (__DEV__) console.log(error, error.stack)

  if (error instanceof AppError) {
    return error.errorCode
  } else {
    return AppError.DUMMY_ERROR
  }
}
