import AppError from '../utils/AppError'
import { delay } from '../utils'

export async function fetchPoloniexTickers() {
  let response = await fetch('https://poloniex.com/public?command=returnTicker', {
    method: 'GET',
    headers: {
      'Content-Type': 'text/json',
    },
  })
  response = await response.json()

  // throw new AppError(AppError.DUMMY_ERROR)
  // await delay(8000)

  return response
}
