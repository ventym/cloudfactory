import React from 'react'
import { Platform, View } from 'react-native'
import { Provider } from 'react-redux'
import { createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation'
import StatusBar from './components/StatusBar'

import styles, { COLORS } from './styles'

import configureStore from './store'
const store = configureStore()

import AboutScreen from './screens/About'
import PoloniexScreen from './screens/Poloniex'

const Navigator = createMaterialTopTabNavigator({
  About: AboutScreen,
  Poloniex: PoloniexScreen,
}, {
  initialRouteName: 'About',
  swipeEnabled: true,
  animationEnabled: true,
})

export default () => (
  <Provider store={store}>
    <View style={[ styles.container, styles.statusBarPadding ]}>
      <StatusBar backgroundColor={COLORS.NAVIGATOR_BACKGROUND} barStyle='light-content' />
      <Navigator />
    </View>
  </Provider>
)
