import React from 'react'
import {
	View,
  FlatList,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import i18n from '../../i18n'
import styles from '../../styles'
import actions from '../../actions'

import LoadingScreen from '../Loading'
import ErrorBox from './ErrorBox'
import TickerItem from './TickerItem'

class PoloniexScreen extends React.Component {
  static navigationOptions = {
    title: i18n.t('poloniex.title'),
  }

  constructor(props) {
    super(props)

    // таймер обновления котировок вынесен в сагу намеренно
    props.navigation.addListener('didFocus', () => {
      props.poloniexDidFocus()
      props.fetchPoloniexTickers()
    })
    props.navigation.addListener('willBlur', () => props.poloniexWillBlur())
  }

  render() {
    if (this.props.poloniex.isLoading && !this.props.poloniex.isLoaded) return <LoadingScreen />

    return (
      <View style={[styles.container]}>
        <ErrorBox errorCode={this.props.poloniex.errorCode} />
        <FlatList
          data={this.props.poloniex.list}
          keyExtractor={item => item}
          renderItem={({ item }) => <TickerItem item={item} key={'pol-'+item} />}
          ItemSeparatorComponent={() => <View style={styles.horizontalLine} />}
        />
      </View>
    )
  }
}

const mapState = (state, ownProps) => ({
  poloniex: state.poloniex,
})

const mapActions = (dispatch) => bindActionCreators({
  fetchPoloniexTickers: actions.poloniex.fetching,
  poloniexDidFocus: actions.poloniex.didFocus,
  poloniexWillBlur: actions.poloniex.willBlur,
}, dispatch)

export default connect(mapState, mapActions)(PoloniexScreen)
