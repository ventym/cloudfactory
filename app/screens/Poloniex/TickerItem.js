import React from 'react'
import {
  Text,
	View,
  Animated,
  Easing,
} from 'react-native'
import { connect } from 'react-redux'

import i18n from '../../i18n'
import styles, { COLORS } from '../../styles'

class TickerItem extends React.Component {
  constructor(props) {
    super(props)
    this.backColor = new Animated.Value(0)
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.ticker.last !== this.props.ticker.last
    || prevProps.ticker.highestBid !== this.props.ticker.highestBid
    || prevProps.ticker.percentChange !== this.props.ticker.percentChange) {
      this.startAnimation()
    }
  }

  render() {
    return (
      <View style={styles.tickerItemContainer}>
        <View style={styles.tickerItemLeftContainer}>
          <Text style={styles.tickerName}>{ this.props.item }</Text>
        </View>
        <Animated.View style={[ styles.tickerItemRightContainer, { backgroundColor: this.backColor.interpolate(this.colorInterpolation) } ]}>
          <Text style={styles.tickerText}>{ i18n.t('poloniex.last') }: { this.props.ticker.last }</Text>
          <Text style={styles.tickerText}>{ i18n.t('poloniex.highestBid') }: { this.props.ticker.highestBid }</Text>
          <Text style={styles.tickerText}>{ i18n.t('poloniex.percentChange') }: { this.props.ticker.percentChange }</Text>
        </Animated.View>
      </View>
    )
  }

  colorInterpolation = {
    inputRange: [0, 0.3, 1],
    outputRange: [COLORS.BACKGROUND, COLORS.HIT, COLORS.BACKGROUND],
  }

  startAnimation = () => {
    const useNativeDriver = false
    // const useNativeDriver = Platform.OS === 'android'
    this.backColor.setValue(0)
    Animated.timing(this.backColor, {
      toValue: 1,
      duration: 1000,
      easing: Easing.in,
      useNativeDriver,
    }).start()
  }
}

const mapState = (state, ownProps) => ({
  ticker: state.poloniex.tickers[ownProps.item],
})

export default connect(mapState)(TickerItem)
