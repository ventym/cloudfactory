import React from 'react'
import {
  Text,
	View,
} from 'react-native'

import i18n from '../../i18n'
import styles from '../../styles'

export default (props) => {
  return props.errorCode ? (
    <View style={styles.errorBox}>
      <Text style={styles.errorText}>{ i18n.t(`error.${props.errorCode}`) }</Text>
    </View>
  ) : null
}
