import React from 'react'
import {
  Text,
	View,
} from 'react-native'

import i18n from '../i18n'
import styles from '../styles'

export default AboutScreen = (props) => (
  <View style={[styles.container, styles.center]}>
    <Text style={styles.h1}>{ i18n.t('about.text') }</Text>
  </View>
)

AboutScreen.navigationOptions = {
  title: i18n.t('about.title'),
}
