import React from 'react'
import {
	View,
  ActivityIndicator,
} from 'react-native'

import styles, { COLORS } from '../styles'

export default () => (
  <View style={[styles.container, styles.center]}>
    <ActivityIndicator size='large' color={COLORS.NAVIGATOR_BACKGROUND} />
  </View>
)
