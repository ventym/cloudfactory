import { takeLatest } from 'redux-saga/effects'

import actions from '../actions'

import { fetchPoloniexTickers } from './poloniex'

export default function* () {
  yield takeLatest(actions.poloniex.fetching, fetchPoloniexTickers)
}
