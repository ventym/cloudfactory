import { call, put, select } from 'redux-saga/effects'
import { delay } from 'redux-saga'

import actions from '../actions'
import * as api from '../api'
import AppError, { prepareError } from '../utils/AppError'

export function* fetchPoloniexTickers(action) {
  try {
    const tickers = yield call(api.fetchPoloniexTickers)
    const list = Object.keys(tickers)

    yield put(actions.poloniex.successful({ tickers, list }))

  } catch(error) {
    yield put(actions.poloniex.rejected(prepareError(error)))
  }

  yield delay(5000)
  let poloniexOnScreen = yield select(state => state.poloniex.onScreen)
  if (poloniexOnScreen) {
    yield put(actions.poloniex.fetching())
  }
}
