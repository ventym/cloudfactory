/* eslint-disable global-require */
/* eslint-disable no-undef */
import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import createSagaMiddleware from 'redux-saga'

import reducer from './reducers'
import rootSaga from './sagas'

export default function configureStore(initialState) {
  const sagaMiddleware = createSagaMiddleware()
  let middleware = [
    sagaMiddleware,
  ]
  if (__DEV__) {
    const reduxImmutableStateInvariant = require('redux-immutable-state-invariant').default()
    middleware = [...middleware, reduxImmutableStateInvariant, logger]
  }

  const store = createStore(
    reducer,
    initialState,
    applyMiddleware(...middleware)
  )

  sagaMiddleware.run(rootSaga) // Start application

  return store
}
